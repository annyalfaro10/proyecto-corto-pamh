
deltaT = Tiempo(2)-Tiempo(1);
PAMH = iddata([Angulo], [Entrada], deltaT);
Motor.inputname = {'Entrada'};
Motor.outputname = {'Salida'};
ident

M=zpk(tf1);
modelo=M(1,1);
y = lsim(modelo,Entrada,Tiempo);
plot(Tiempo, Entrada, Tiempo, Angulo, Tiempo, y)
grid
title('Verificaci�n de la respuesta del modelo')
xlabel('Tiempo [s]')
ylabel('Salida')
legend('Entrada','Salida','Modelo')

% FILTRO DE MUESCA    No cumple con los requerimientos sin embargo si se
% trabaja como un I_PD con la KP positiva y un N=4 s� funciona bien
s= tf('s');
tfani=tf(0.62363/(s^2 + 0.149*s+0.9413));
wn=0.9702;
chi= 4/(wn*20);
fma=(s^2 + 2*chi*wn*s +wn^2)/wn^2;
a=3;
tfina=(tfani*(1/a))^-1;
fmb=(a*wn)/(s+a*wn);
fmc=(wn/a) /(s+ wn/a);
CFM=tfina*fmb*fmc
%KD=4.006
%KI=1.318
%Kp=-1.7518
%N=0.6723


%wn a pata 
y1= 1.18-0.663;
y2= 0.976-0.663;
delta= log(y1/y2);
chi= delta/sqrt((2*pi)^2 + delta^2);
f=2*pi/abs(9.88-3.24);
w= 2*pi*f;
wn= w/sqrt(1-chi^2);


%REI    Este NO FUNCIONO 
%descubrir donde ubicar los polos
s= tf('s');
tfani=tf(0.62363/(s^2 + 0.149*s+0.9413));
tfanib= feedback(tfani,1);
wn=0.9702;
chi= 4/(wn*20);
Ps=[(-1*chi*wn)+i*(sqrt(1-chi^2)) (-1*chi*wn)-i*(sqrt(1-chi^2))];
tf0=tf;
clear tf;

%calcular K y ki
rei=ss(tfani);
As=[rei.A [0;0]; -1*rei.C 0];
Bs=[rei.B ; 0];
Ms=[Bs As*Bs As^2*Bs];
polis=(s-Ps(1))*(s-Ps(2))

fiAs=As^2 + 0.4 *As + 0.9975*eye(3); % modelar el fiAs de acuerdo a polis
Ks=[0 0 1] * inv(Ms)* fiAs ;
K= Ks(1:2)
ki=-Ks(3)
%KI=1.9548
%KP=-0.4321
%KD=2.0356



%Valores IMC
%Kp=-0.0954
%Ki=0.3
%Kd=0.364
%N=2.1008


% LQR REI   SI FUNCIONO 
s= tf('s');
tfani=tf(0.62363/(s^2 + 0.149*s+0.9413));
ssan=ss(tfani);
Q= eye(3);
R= eye(1);
A=ssan.A;
B=ssan.B;
C=ssan.C;
D=ssan.D;
As=[ssan.A [0;0]; -1*ssan.C 0];
Bs=[ssan.B ; 0];
[Ks S E] = lqr(As, Bs, Q, R); %E es donde deberian ir los polos para la estabilidad del sistema
K= Ks(1:2)
ki=-Ks(3)

CLQR= zpk([-0.3318 + 0.5200i -0.3318 - 0.5200i], [0], [2.6283])
%se ingresa la matriz K=[1.6391    1.0876] y ki=1
%KP=1.6391
%KD=1.0876
%KI=1


%I_PD           Quizas s� funciono
s= tf('s');
tfani=tf(0.62363/(s^2 + 0.149*s+0.9413));

%de manera siguiente escoja cual wn elegir si el de anibal o el calculado
%mi sugerencia es el de anibal

%wn y chi a pata
y1= 1.18-0.663;
y2= 0.976-0.663;
delta= log(y1/y2);
chi= delta/sqrt((2*pi)^2 + delta^2);
f=2*pi/abs(9.88-3.24);
w= 2*pi*f;
wn= w/sqrt(1-chi^2);

%wn a ojo de tfani
wn=0.9702;
chi= 4/(wn*20);

Ps=[(-1*chi*wn)+i*(sqrt(1-chi^2)) (-1*chi*wn)-i*(sqrt(1-chi^2))];
ipd=ss(tfani);

A=[0 1; -1*ipd.A(1,1) -1*ipd.A(1,2)];
B=[0;ipd.C(1,2)];
C=[1 0];
As=[A [0;0]; -1*C 0];
Bs=[B ; 0];
Ms=[Bs As*Bs As^2*Bs];
polis=(s-Ps(1))*(s-Ps(2))

fiAs=As^2 + 0.4 *As + 0.9975*eye(3); % modelar el fiAs de acuerdo a polis
Ks=[0 0 1] * inv(Ms)* fiAs ;
K= Ks(1:2)
ki=-Ks(3)
%wn=5.9645
%Kp=    1.5229    KD= 1.6035    KI= 1.9547

%wn=0.9702
%Kp= 0.6414       KD=1.6035     KI=1.5995

%wn=25.

